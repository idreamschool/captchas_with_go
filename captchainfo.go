//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"time"
)

// CaptchaInfo is the entity of a captcha
// text:the content text,for the image display and user to recognize
// createTime:the time when the captcha is created
type CaptchaInfo struct {
	Text       string
	CreateTime time.Time
	ShownTimes int
}
