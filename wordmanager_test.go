//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"os"
	"testing"
)

func TestWordManager(t *testing.T) {
	pwd, err := os.Getwd()
	if (nil != err) || "" == pwd {
		return
	}
	path := pwd + "/data/"
	singleCharDict := []string{"cn_char", "en_char"}
	phrasesDict := []string{"cn_phrases", "en_phrases"}
	length := 6

	for _, f := range singleCharDict {
		mgr, err := CreateWordManagerFromDataFile(path + f)
		s, err := mgr.Get(length)
		if nil != err {
			t.Errorf(err.Error())
		} else if length != len([]rune(s)) {
			t.Errorf("get no equals length:" + f)
		}
	}
	for _, f := range phrasesDict {
		mgr, err := CreateWordManagerFromDataFile(path + f)
		s, err := mgr.Get(length)
		if nil != err {
			t.Errorf(err.Error())
		} else if 0 >= len([]rune(s)) {
			t.Errorf("not get:" + f)
		}
	}

}
